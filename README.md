# Reverse Rug Monorepo

## About

The Reverse Rug Monorepo is a repository of tools for the preparation, creation, and distribution of NFTs (non-fungible tokens) on the Solana network.

Although it was originally designed for the purpose of reversing rug pulls, we believe that the tools are applicable to a number of use cases which also apply to new and ongoing NFT projects which aren't reverse rugs. Please use it in any way which you find beneficial to your project.

If you are unfamiliar with monorepos, [Gregory Szorc has a great writeup](https://gregoryszorc.com/blog/2014/09/09/on-monolithic-repositories/). Basically, instead of every tool having a separate repository in Git, we have all of the tools in a single repository which allows them to be developed together, tested together, and used together without worrying about matching up the versions.

If you support our work, consider [joining our community on Discord](https://discord.gg/kitten-coup), [following our Twitter account](https://twitter.com/KittenCoupNFT), or purchasing a Kitten Coup NFT on [Magic Eden](https://magiceden.io/marketplace/kittencoup) or [Alpha Art](https://alpha.art/collection/kitten-coup?Status=-BUY_NOW).

## Tools



## Running



## License

```
Reverse Rug Monorepo is free software: you can redistribute it and/or 
modify it under the terms of the GNU General Public License as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
